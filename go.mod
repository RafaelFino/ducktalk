module ducktalk

go 1.14

require (
	github.com/gizak/termui/v3 v3.1.0
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/nats-io/stan.go v0.6.0
)
