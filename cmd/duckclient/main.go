package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"

	"ducktalk/internal/common"
	"ducktalk/internal/connection"
	"ducktalk/internal/room"

	ui "github.com/gizak/termui/v3"
	"github.com/gizak/termui/v3/widgets"
)

func main() {
	//read config
	cfg, err := readConfig()

	if err != nil {
		panic(err)
	}

	//init logger
	f, err := os.OpenFile(cfg.LogFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)

	if err != nil {
		panic(err)
	}

	defer f.Close()
	log.SetOutput(f)

	log.Printf("Config: %v\n", cfg)

	//init ui manager
	input, list := initUI(cfg)

	//connect on nats-stream
	conn := connection.New(cfg)
	//subscribe channel
	ch, err := conn.Subscribe()

	if err != nil {
		log.Fatalf("fail to subscribe channel: %s\n", err)
	}

	//handle with users and messages
	r := room.New(cfg, ch)

	//handle with defer resources
	defer func(r *room.Room, c *connection.Connection) {
		log.Println("Closing connections and elements")

		ui.Close()
		r.Close()
		conn.Close()

		log.Println("Stop!")
	}(r, conn)

	//handle receive messages
	go receive(r.Output, list, cfg)

	//handle term events and hold execution
	termEventHandler(r, conn, input, list)
}

func readConfig() (*common.Config, error) {
	var raw []byte
	var err error

	if raw, err = ioutil.ReadFile(os.Args[1]); err != nil {
		return nil, err
	}

	var cfg common.Config

	err = json.Unmarshal(raw, &cfg)

	if len(cfg.Database) == 0 {
		cfg.Database = os.Args[0] + "." + cfg.Nickname + ".db"
	}

	if len(cfg.LogFile) == 0 {
		cfg.LogFile = os.Args[0] + "." + cfg.Nickname + ".log"
	}

	return &cfg, err
}

func initUI(cfg *common.Config) (*widgets.Paragraph, *widgets.List) {
	if err := ui.Init(); err != nil {
		log.Fatalf("failed to initialize termui: %v\n", err)
	}

	//creates ui elements
	input := widgets.NewParagraph()
	input.Title = "[" + cfg.Nickname + "]"
	input.BorderStyle = ui.NewStyle(
		ui.ColorYellow,
	)

	input.TitleStyle = ui.NewStyle(
		ui.ColorYellow,
		ui.ColorClear,
		ui.ModifierBold,
	)

	list := widgets.NewList()
	list.Rows = []string{}
	list.Title = "[" + cfg.Subject + "]"
	list.BorderStyle = ui.NewStyle(
		ui.ColorBlue,
	)
	list.TitleStyle = ui.NewStyle(
		ui.ColorBlue,
		ui.ColorClear,
		ui.ModifierBold,
	)

	w, h := ui.TerminalDimensions()
	drawUI(input, list, w, h)

	return input, list
}

func termEventHandler(r *room.Room, conn *connection.Connection, input *widgets.Paragraph, list *widgets.List) {
	var err error

	for e := range ui.PollEvents() {
		switch e.ID {
		case "<Resize>":
			payload := e.Payload.(ui.Resize)
			drawUI(input, list, payload.Width, payload.Height)
		case "<Escape>":
			log.Println("Quit requested")
			return
		case "<Enter>":
			if len(input.Text) == 0 {
				break
			}
			log.Printf("Send message: %s\n", input.Text)
			err = conn.SendMessage(input.Text)

			if err != nil {
				log.Printf("send error: %s\n", err)
			}

			input.Text = ""
		case "<Backspace>", "<C-<Backspace>>":
			if len(input.Text) > 1 {
				input.Text = input.Text[0 : len(input.Text)-1]
			} else {
				input.Text = ""
			}
		case "<Delete>":
			if len(input.Text) > 1 {
				input.Text = input.Text[1:len(input.Text)]
			} else {
				input.Text = ""
			}
		case "<C-c>":
			input.Text = ""
		case "<C-l>":
			r.ListUsers()
		case "<Space>":
			input.Text += " "
		case "<Tab>":
			input.Text += "\t"
		case "<PageUp>":
			list.ScrollTop()
			ui.Render(list)
		case "<PageDown>":
			list.ScrollBottom()
			ui.Render(list)
		case "<Up>":
			list.ScrollUp()
			ui.Render(list)
		case "<Down>":
			list.ScrollDown()
			ui.Render(list)
		default:
			if e.Type == ui.KeyboardEvent {
				if v, found := common.TbKeys[e.ID]; found {
					input.Text += v
				} else {
					input.Text += e.ID
				}
			}
		}

		ui.Render(input)
	}
}

func receive(ch chan string, list *widgets.List, cfg *common.Config) {
	for m := range ch {
		list.Rows = append(list.Rows, m)
		if len(list.Rows) > cfg.MsgBoxBufferSize {
			list.Rows = list.Rows[len(list.Rows)-cfg.MsgBoxBufferSize:]
		}

		list.ScrollBottom()
		ui.Render(list)

		log.Printf("received message: %s\n", m)
	}
}

func drawUI(input *widgets.Paragraph, list *widgets.List, w int, h int) {
	input.SetRect(0, h-3, w, h)
	list.SetRect(0, 0, w, h-3)

	ui.Render(input)
	ui.Render(list)
}
