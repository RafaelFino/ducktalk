package room

import (
	"fmt"
	"log"
	"strings"

	"ducktalk/internal/common"
	"time"
)

type Room struct {
	users    map[string]time.Time
	received chan *common.Message
	messages []*common.Message
	holder   chan bool
	cfg      *common.Config
	Output   chan string
}

//New Creates a new chat room and handle messages
func New(cfg *common.Config, ch chan *common.Message) *Room {
	room := &Room{
		received: ch,
		users:    map[string]time.Time{},
		messages: []*common.Message{},
		cfg:      cfg,
		Output:   make(chan string, 2000),
		holder:   make(chan bool),
	}

	go room.processMessages()
	go room.autoKick()

	return room
}

func (r *Room) Close() {
	r.holder <- true
}

func (r *Room) ListUsers() {
	ret := []string{}

	for u := range r.users {
		ret = append(ret, u)
	}

	r.Output <- fmt.Sprintf("[%s] Active users: %s", time.Now().Format("2006-01-02 15:04:05"), strings.Join(ret, ", "))
}

func (r *Room) processMessages() {
	for msg := range r.received {
		switch msg.Type {
		case common.Leave:
			if _, found := r.users[msg.From]; found {
				delete(r.users, msg.From)
			}
			r.receiveMessage(msg)
		case common.UserMessage, common.Enter:
			r.receiveMessage(msg)
			r.users[msg.From] = time.Now()
		case common.KeepAlive:
			log.Printf("Keep alive received from %s\n", msg.From)
			r.users[msg.From] = time.Now()
		}
	}
}

func (r *Room) autoKick() {
	for {
		select {
		case <-time.After(time.Second):
			for u, t := range r.users {
				if time.Since(t) > (time.Minute) {
					r.receiveMessage(&common.Message{
						When:    time.Now(),
						Data:    u + " is no longer active",
						Subject: r.cfg.Subject,
						From:    u,
						Type:    common.Kick,
					})
					delete(r.users, u)
				}
			}
		case <-r.holder:
			return
		}
	}
}

func (r *Room) receiveMessage(msg *common.Message) {
	switch msg.Type {
	case common.Enter, common.Leave, common.Kick:
		r.Output <- fmt.Sprintf("[%s] %s", msg.When.Format("2006-01-02 15:04:05"), msg.Data)
	default:
		r.Output <- fmt.Sprintf("[%s] %s: %s", msg.When.Format("2006-01-02 15:04:05"), msg.From, msg.Data)
	}
}
