package connection

import (
	"ducktalk/internal/common"
	"ducktalk/internal/store"
	"encoding/json"
	"fmt"
	"log"
	"time"

	stan "github.com/nats-io/stan.go"
)

//Connection internal struct
type Connection struct {
	subject        string
	nickname       string
	serverURI      string
	clusterID      string
	receiveChannel chan *common.Message
	conn           stan.Conn
	storage        *store.Storage
}

//New creates a connection
func New(cfg *common.Config) *Connection {
	ret := &Connection{
		serverURI:      cfg.ServerURI,
		receiveChannel: make(chan *common.Message, 2000),
		subject:        cfg.Subject,
		nickname:       cfg.Nickname,
		clusterID:      cfg.ClusterID,
	}

	storage, err := store.New(cfg.Database)

	if err != nil {
		log.Fatalf("fail to try open database on %s", cfg.Database, err)
	}

	ret.storage = storage

	if pastMessages, err := ret.storage.Read(cfg.Subject, cfg.Nickname); err == nil {
		for _, m := range pastMessages {
			ret.receiveChannel <- m
		}
	} else {
		log.Fatalf("error to try recovery past messages, err: %s", err)
	}

	return ret
}

//Subscribe subscribe a subject
func (c *Connection) Subscribe() (chan *common.Message, error) {
	log.Printf("Trying to connect on %s\n", c.serverURI)
	sc, err := stan.Connect(c.clusterID, fmt.Sprintf("%s_%s", c.subject, c.nickname), stan.NatsURL(c.serverURI))

	if err != nil {
		return nil, err
	}

	log.Printf("Trying to subscribe on %s\n", c.subject)
	_, err = sc.Subscribe(
		c.subject,
		c.receiveMessage,
		stan.StartAtTimeDelta(2*time.Hour),
		stan.DurableName(c.subject),
	)

	if err != nil {
		return nil, err
	}

	c.conn = sc

	if err = c.sendEnter(); err != nil {
		log.Fatalf("subscribe error: %s\n", err)
		return c.receiveChannel, err
	}

	log.Printf("Connected on %s\n", c.serverURI)
	go c.keepAlive()

	return c.receiveChannel, err
}

//SendMessage Send an user message
func (c *Connection) SendMessage(data string) error {
	raw, err := json.Marshal(common.Message{
		When:    time.Now(),
		Data:    data,
		Subject: c.subject,
		From:    c.nickname,
		Type:    common.UserMessage,
	})

	if err != nil {
		log.Fatal(err)
		return err
	}

	return c.conn.Publish(c.subject, raw)
}

func (c *Connection) sendEnter() error {
	raw, err := json.Marshal(common.Message{
		When:    time.Now(),
		Data:    c.nickname + " is here!",
		Subject: c.subject,
		From:    c.nickname,
		Type:    common.Enter,
	})

	if err != nil {
		log.Fatal(err)
		return err
	}

	return c.conn.Publish(c.subject, raw)
}

func (c *Connection) keepAlive() {
	for c.conn != nil {
		time.Sleep(45 * time.Second)

		raw, err := json.Marshal(common.Message{
			When:    time.Now(),
			Subject: c.subject,
			From:    c.nickname,
			Type:    common.KeepAlive,
		})

		if err != nil {
			log.Fatal(err)
		}

		log.Printf("Publishing keep alive on %s\n", c.serverURI)
		c.conn.Publish(c.subject, raw)
	}
}

func (c *Connection) sendLeave() error {
	raw, err := json.Marshal(common.Message{
		When:    time.Now(),
		Data:    c.nickname + " quit",
		Subject: c.subject,
		From:    c.nickname,
		Type:    common.Leave,
	})

	if err != nil {
		log.Fatal(err)
		return err
	}

	return c.conn.Publish(c.subject, raw)
}

//Close Close streaming connection
func (c *Connection) Close() {
	log.Printf("Closing nats-stream connection")

	if c.conn != nil {
		if err := c.sendLeave(); err != nil {
			panic(err)
		}

		c.conn.Close()
		c.conn = nil
		close(c.receiveChannel)
	}
}

func (c *Connection) receiveMessage(msg *stan.Msg) {
	var received common.Message

	if err := json.Unmarshal(msg.Data, &received); err == nil {
		received.ID = msg.Sequence
		c.receiveChannel <- &received

		if received.Type != common.KeepAlive {
			if err = c.storage.Write(&received); err != nil {
				log.Fatalf("fail to try write message on db, msg: %v, err: %s", received, err)
			}
		}
	}

	msg.Ack()
}
