package common

import (
	"time"
)

type Message struct {
	When    time.Time   `json:"when"`
	Type    MessageType `json:"type"`
	Subject string      `json:"subject"`
	From    string      `json:"from"`
	Data    string      `json:"data"`
	ID      uint64      `json:"id"`
}

type Config struct {
	ScreenSize       int    `json:"screen-size"`
	MsgBoxSize       int    `json:"msg-box-size"`
	MsgBoxBufferSize int    `json:"msg-box-buffer-size"`
	ServerURI        string `json:"server-uri"`
	ClusterID        string `json:"cluster-id"`
	Subject          string `json:"subject"`
	Nickname         string `json:"nickname"`
	Database         string `json:"database-file"`
	LogFile          string `json:"log-file"`
}

type MessageType int

const (
	Enter MessageType = iota
	UserMessage
	Leave
	KeepAlive
	Kick
	ListUsers
)

var TbKeys = map[string]string{
	"<F1>":        "",
	"<F2>":        "",
	"<F3>":        "",
	"<F4>":        "",
	"<F5>":        "",
	"<F6>":        "",
	"<F7>":        "",
	"<F8>":        "",
	"<F9>":        "",
	"<F10>":       "",
	"<F11>":       "",
	"<F12>":       "",
	"<Insert>":    "",
	"<Home>":      "",
	"<End>":       "",
	"<PageUp>":    "",
	"<PageDown>":  "",
	"<Up>":        "",
	"<Down>":      "",
	"<Left>":      "",
	"<Right>":     "",
	"<C-<Space>>": "",
	"<C-a>":       "",
	"<C-b>":       "",
	"<C-d>":       "",
	"<C-e>":       "",
	"<C-f>":       "",
	"<C-g>":       "",
	"<C-j>":       "",
	"<C-k>":       "",
	"<C-n>":       "",
	"<C-o>":       "",
	"<C-p>":       "",
	"<C-q>":       "",
	"<C-r>":       "",
	"<C-s>":       "",
	"<C-t>":       "",
	"<C-u>":       "",
	"<C-v>":       "",
	"<C-w>":       "",
	"<C-x>":       "",
	"<C-y>":       "",
	"<C-z>":       "",
	"<C-4>":       "",
	"<C-5>":       "",
	"<C-6>":       "",
	"<C-7>":       "",
}
