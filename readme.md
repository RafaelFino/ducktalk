# Ducktalk - a son from COVID-19
> Something very simple just done to entertain my bored child

A very simple terminal chat, ~~without~~ with a little persistance and ~~any~~ some simple other controls, just for coding, just for fun.

~~Without~~ With log files.

Using a ~~nats~~ nats-stream as server, running on ~~an ODROID C2~~ a nano instance on GCP

Storing data on a local Sqlite3 db

## To run:

```sh
go build -o ./bin/client ./cmd/duckclient/main.go && ./bin/client ./config/config.json [nickname]
```
